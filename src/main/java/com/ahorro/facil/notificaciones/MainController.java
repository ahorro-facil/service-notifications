package com.ahorro.facil.notificaciones;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/saludo")
public class MainController {
    @GetMapping
    public String saludo(){
        return "Hola notificationes";
    }
}
