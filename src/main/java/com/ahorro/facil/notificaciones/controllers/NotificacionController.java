package com.ahorro.facil.notificaciones.controllers;
import java.util.List;



import com.ahorro.facil.notificaciones.model.Notificacion;
import com.ahorro.facil.notificaciones.repositories.NotificacionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/notificationes")

public class NotificacionController {

     @Autowired
     NotificacionRepository NotificacionRepository;
    

    //  @GetMapping("/noty")
    //  public ResponseEntity<List<Notificacion>> findNotificacionByUserId(@RequestParam(required = true) long id){
    //     List<Notificacion> notificacion = NotificacionRepository.findByUserId(id);
    //     return new ResponseEntity<>(notificacion,HttpStatus.OK);
         
    //  }
    @GetMapping("/noty")
    public ResponseEntity<List<Notificacion>> findNotificacionByUserId(@RequestParam(required = true) long id){
        List<Notificacion> notificacions = NotificacionRepository.findByUserId(id);
        return new ResponseEntity<>(notificacions, HttpStatus.OK);
    }

     @PostMapping("/noty")
     public ResponseEntity<Notificacion> registerNotificacion(@RequestBody Notificacion notificacion){
         try{
          

             Notificacion created = NotificacionRepository.save(
                 new Notificacion(notificacion.getTitle(), notificacion.getMessage(), notificacion.getUserId()));
                 return new ResponseEntity<>(created, HttpStatus.CREATED);
         } catch(Exception e){
             return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

         }
      

     }

 }
