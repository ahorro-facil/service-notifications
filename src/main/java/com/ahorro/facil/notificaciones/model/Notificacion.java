package com.ahorro.facil.notificaciones.model;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "notificacion")
public class Notificacion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "message")
    private String message;

    @Column(name = "user_id")
    private long userId;

    public Notificacion(String title, String message, long userId) {
        this.title = title;
        this.message = message;
        this.userId = userId;
    }  
}
