package com.ahorro.facil.notificaciones.repositories;

 import java.util.List;

 import com.ahorro.facil.notificaciones.model.Notificacion;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


 public interface NotificacionRepository  extends JpaRepository<Notificacion, Long>{
    
     @Query(
         value = "SELECT * FROM notificacion i WHERE i.user_id = ?1",
         nativeQuery = true
     )
     List<Notificacion> findByUserId(long userId);
 }
